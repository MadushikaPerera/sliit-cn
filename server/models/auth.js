const AuthModel = (function() {
	
	function register(firstname, lastname, username, password, sliitreg, mongoose, cb) {
		const user = mongoose.model('register', { 'firstname': String, 'lastname': String, 'username': String, 'password': String, 'sliitreg': String });

		newUser = {
			firstname: firstname,
			lastname: lastname,
			username: username,
			password: password,
			sliitreg: sliitreg
		}

		register = new user(newUser);

		register.save(function(err) {
			if(err)	cb(null, err);

			else {
				cb(true, null);
			}
		});
	}

	return {
		register: register
	};
})();

module.exports = AuthModel;