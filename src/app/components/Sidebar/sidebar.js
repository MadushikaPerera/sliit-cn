import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import { Link } from 'react-router'

const DrawerCustom = React.createClass({
	getInitialState: function() {
		return {
			open: false,
			owner: true
		}
	},

	handleClose: function() {
		this.props.close();
	},

	render: function() {
		return (			
			<div>
		        <Drawer
		          docked={false}
		          width={300}
		          open={this.props.visibility}
		          onRequestChange={(open) => this.setState({open})}
		        >
		        	{
		        		this.state.owner ? 
		        			<div>
			        			<MenuItem onTouchTap={this.handleClose}><Link to={`/dashboard`}>Dashboard</Link></MenuItem>
			        			<MenuItem onTouchTap={this.handleClose}><Link to={`/foods`}>Foods </Link></MenuItem>
			        			<MenuItem onTouchTap={this.handleClose}><Link to={`/cashiers`}>Cashiers </Link></MenuItem>
			        			<MenuItem onTouchTap={this.handleClose}><Link to={`/customers`}>Customers </Link></MenuItem>
			        		</div>
		        			: ''
		        	}
		        	

		        </Drawer>
		    </div>
		)
	}
});

export default DrawerCustom;